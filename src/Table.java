public class Table {
    
    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player player1;
    private Player player2;
    private int row;
    private int col;
    private int countTurn = 0;

    public Table(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }
    
    public char[][] getTable() {
        return table;
    }
    
    public boolean setRowCol(int row, int col) {
        if(table[row][col] != '-'){
            return false;
        }
        table[row][col] = currentPlayer.getSymbol();
        this.row = row;
        this.col = col;
        return true;
    }
    
    public boolean checkWin() {
        if(checkRow()) {
            return true;
        }if(checkCol()) {
            return true;
        }if(checkDiagonaLeft()) {
            return true;
        }if(checkDiagonalRight()) {
            return true;
        }
        return false;
    }
    
    public boolean checkDraw(){
        if(checkWin() == false) {
            countTurn++;
        if(countTurn != 9) {
            return false;
        }
        }return true;
    }
    
    private boolean checkRow(){
        for(int i=0; i<3; i++) {
            if(table[row][i] != currentPlayer.getSymbol()){
                return false;
            }
        }return true;
    }
    
    public boolean checkCol(){
        for(int i=0; i<3; i++) {
            if(table[i][col] != currentPlayer.getSymbol()) {
                return false;
            }
        }return true;
    }
    
    public boolean checkDiagonaLeft(){
        for(int i=0; i<3; i++) {
            if(table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }return true;
    }
    
    public boolean checkDiagonalRight(){
        if(table[0][2] != currentPlayer.getSymbol() || table[1][1] != currentPlayer.getSymbol() || table[2][0] != currentPlayer.getSymbol()) {
            return false;
        }return true;
    }
    
    
    public Player switchPlayer() {
        if(currentPlayer == player1){
            return currentPlayer = player2;
        }else{
            return currentPlayer = player1;
        }
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public char[][] resetTable(){
        for(int i=0; i<3; i++){
            for(int j=0; j<3; j++){
                table[i][j] = '-';
            }
        }
        return table;
    }

}