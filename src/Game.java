import java.util.Scanner;

public class Game {
    
    private Table table;
    private Player player1, player2;
    
    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void Play() {
        showWelcome();
        newGame();
        while (true) {
            showTable();
            showTurn();
            inputRowCol();
            if (table.checkWin()) {
                showTable();
                showWin();
                showContinue();
                if(checkContinue()){
                    newTable();
                    newGame();
                    continue;
                }else{
                    showGameOver();
                    break;
                }                
            }if(table.checkDraw()){
                showTable();
                showDraw();
                showContinue();
                if(checkContinue()){
                    newTable();                    
                    newGame();
                    continue;
                }else{
                    showGameOver();
                    break;
                }                
            }
            table.switchPlayer();
            
        }

    }

    public void showWelcome() {
        System.out.println("Welcome to XOXO Game!!!");
    }
    
    public void showTable() {
        System.out.println("-------------");        
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");        

        }
    }

    public void newGame() {
        table = new Table(player1, player2);

    }

    public void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("*** Please input row, col: ");
            int row = kb.nextInt()-1;
            int col = kb.nextInt()-1;
            if (table.setRowCol(row, col)) {
                table.setRowCol(row, col);               
                break;
            }
        }
    }

    public void showTurn() {
        System.out.println("Player " + table.getCurrentPlayer().getSymbol()+" turn");
    }
    
    public void showWin(){
        
        System.out.println("Player "+table.getCurrentPlayer().getSymbol() + " win!!!");
    }
    
    public void showDraw(){
        System.out.println("It's Draw!!!");
    }
    
    public void showContinue(){
        System.out.print("Do you want to play again? [y/n]: ");
    }
    
    public void showGameOver(){
        System.out.println("!!!Game Over!!!");
        System.out.println("*** BYE BYE ***");

    }
    public boolean checkContinue(){
        Scanner kb = new Scanner(System.in);
        String ans = kb.next();
        if(ans.equals("y")){
            return true;
        }else if(ans.equals("n")){
            return false;
        }return false;
    }
    
    public void newTable() {
        char[][] t = table.resetTable();
    }
 

}